# hard-link-directory
Allows to create a hard link for directories

Compiling:

gcc -o linkhard.o linkhard.c -Wall

gcc -o unlinkhard.o unlinkhard.c

Rules:
1. The file system must be journaled HFS+.
1. The parent directories of the source and destination must be different.
1. The source’s parent must not be the root directory.
1. The destination must not be in the root directory.
1. The destination must not be a descendent of the source.
1. The destination must not have any ancestor that’s a directory hard link.
