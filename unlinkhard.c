#include <stdio.h>
#include <unistd.h>
int main(int argc, char *argv[]) {
   if (argc != 2) {
       perror("Invalide parameters!");
       return 1;
   }
   
   int ret = unlink(argv[1]);
   if (ret != 0) {
       perror("Internal call of \"unlink\" was ended with an error!");
   }
   
   return ret;
}